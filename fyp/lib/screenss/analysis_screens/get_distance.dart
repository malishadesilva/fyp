import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:fyp/screenss/analysis_screens/distance_analysis.dart';
import 'package:fyp/utils/database.dart';
import 'package:fyp/widgets/database/crud/db_item_list.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class GetDistance extends StatefulWidget {
  const GetDistance({Key key}) : super(key: key);

  @override
  _GetDistanceState createState() => _GetDistanceState();
}

class _GetDistanceState extends State<GetDistance> {
  List<double> distanceList;
  List<String> distanceListString;

  List<dynamic> distanceTaskData;

  @override
  void initState() {
    getDistanceFromGPSPointsInRoute();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.refresh_outlined),
        onPressed: () => getDistanceFromGPSPointsInRoute(),
      ),
      appBar: AppBar(
        title: Text("Task list accending by distace"),
      ),
      body: distanceTaskData.length == 0
          ? Text(" ")
          : ListView.builder(
              shrinkWrap: true,
              itemCount: distanceTaskData.length,
              itemBuilder: (context, index) {
                return Card(
                  margin: EdgeInsets.all(15),
                  color: Colors.green[100],
                  elevation: 2,
                  child: ListTile(
                    leading: Text((index + 1).toString()),
                    title: Text(distanceTaskData[index][4].toString()),
                    subtitle: Column(
                      // mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment : CrossAxisAlignment.start,
                      children: [
                        Text('Distance : ${distanceTaskData[index][0].toString()}'),
                        Text(distanceTaskData[index][2].toString()),
                      ],
                    ),
                    trailing: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        // IconButton(onPressed: () {}, icon: Icon(Icons.edit)),
                      ],
                    ),
                  ),
                );
              }),
    );
  }

//distace calculate algorythm
  Future<void> getDistanceFromGPSPointsInRoute() async {
    var currentLatLan = await _getCurrentPosition();
    // print('Cureent location : $currentLatLan');
    double currentLat = currentLatLan[0]["lat"];
    double currentLng = currentLatLan[0]["lng"];
    double locationDistance = 0.0;
    String locationDistancebyString = "";
    double calculateDistance(lat1, lon1, lat2, lon2) {
      var p = 0.017453292519943295;
      var c = cos;
      var a = 0.5 -
          c((lat2 - lat1) * p) / 2 +
          c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
      return 12742 * asin(sqrt(a));
    }

    List<dynamic> data = [
      // {"lat": 6.913612893741779, "lng": 79.89345034507117},
      // {"lat": 6.2262004698425235, "lng": 80.06982129005817},
      // {"lat": 6.2262004698425235, "lng": 80.06982129005817},
      // {"lat": 6.1262004698425235, "lng": 80.36982129005817},
    ];

    data = await Database.readLanLon();
    print('addd $data');
    distanceList = [];
    distanceListString = [];
    distanceTaskData = [];

    for (var i = 0; i < data.length; i++) {
      print("chek");
      //distance cal
      locationDistance = calculateDistance(
          data[i]["lat"], data[i]["lng"], currentLat, currentLng);
      print('ddfq $locationDistance');
      locationDistance = locationDistance / 1000;
      distanceList.add(locationDistance);
      distanceListString.add(locationDistance.toStringAsFixed(2));

      distanceTaskData.add([
        '${locationDistance.toStringAsFixed(2)} km',
        data[i]["country"],
        data[i]["address"],
        data[i]["title"],
        data[i]["description"],
        data[i]["date"],
        data[i]["cost"]
      ]);
    }
    print('ggggg $distanceTaskData');
    setState(() {
      this.distanceList = distanceList;
      this.distanceListString = distanceListString;
    });
    // return distanceList;
  }

//get Current Location
  final GeolocatorPlatform _geolocatorPlatform = GeolocatorPlatform.instance;
  Future<List<dynamic>> _getCurrentPosition() async {
    final position = await _geolocatorPlatform.getCurrentPosition();
    double lat = position.latitude;
    double lng = position.longitude;
    List<dynamic> data = [
      {"lat": lat, "lng": lng}
    ];
    return data;
  }

//Location accuracy algorthem

//  void _getLocationAccuracy() async {
//     final status = await _geolocatorPlatform.getLocationAccuracy();
//     _handleLocationAccuracyStatus(status);
//   }

//   void _requestTemporaryFullAccuracy() async {
//     final status = await _geolocatorPlatform.requestTemporaryFullAccuracy(
//       purposeKey: "TemporaryPreciseAccuracy",
//     );
//     _handleLocationAccuracyStatus(status);
//   }

//   void _handleLocationAccuracyStatus(LocationAccuracyStatus status) {
//     String locationAccuracyStatusValue;
//     if (status == locationAccuracyStatusValue) {
//       locationAccuracyStatusValue = 'Precise';
//     } else if (status == LocationAccuracyStatus.reduced) {
//       locationAccuracyStatusValue = 'Reduced';
//     } else {
//       locationAccuracyStatusValue = 'Unknown';
//     }

//   }

}
