import 'package:flutter/material.dart';
import 'package:fyp/widgets/analysis_widgets/anaylsis_list_item.dart';

class PredictionScreen extends StatefulWidget {
  const PredictionScreen({Key key}) : super(key: key);

  @override
  _PredictionScreenState createState() => _PredictionScreenState();
}

class _PredictionScreenState extends State<PredictionScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.lime,
      appBar: AppBar(
        title: Text("Prediction Screen"),
      ),
      body: ListView(
        children: [
          SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.all(18.0),
            child: Text("My Budget",  style: TextStyle(
                        color: Colors.black,
                        fontSize: 28,
                        fontWeight: FontWeight.w800),
                  ),
          ),
           Padding(
            padding: const EdgeInsets.all(20.0),
            child: Text(
              "14323.50 LKR",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 25,
                  fontWeight: FontWeight.w800),
            ),
          ),
          SizedBox(
            height: 80,
          ),
          Container(
            width: double.infinity,
            height: MediaQuery.of(context).size.height - 200,

            //margin: EdgeInsets.symmetric(horizontal: 20),
            padding: EdgeInsets.all(30),
            child: ListView(
              children: [
                Text(
                  "Monthly Cost",
                  style: TextStyle(
                      color: Colors.black54,
                      fontSize: 20,
                      fontWeight: FontWeight.w800),
                ),
                Divider(),
                Text(
                  "January",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 20,
                      fontWeight: FontWeight.w800),
                ),
          AnaylysisListItem(title: "Food",amount : 120.50),
               AnaylysisListItem(title: "Travel", amount: 4120.50),
                Divider(),
                Text(
                  "February",
                  style: TextStyle(
                      color: Colors.black54,
                      fontSize: 20,
                      fontWeight: FontWeight.w800),
                ),
                 AnaylysisListItem(title: "Food",amount : 1220.50),
               AnaylysisListItem(title: "Travel", amount: 3120.50),
                Divider(),
                Text(
                  "March",
                  style: TextStyle(
                      color: Colors.black54,
                      fontSize: 20,
                      fontWeight: FontWeight.w800),
                ),
                 AnaylysisListItem(title: "Food", amount: 343.50),
                AnaylysisListItem(title: "Travel", amount: 2323.50),
                Divider(),
                Text(
                  "April",
                  style: TextStyle(
                      color: Colors.black54,
                      fontSize: 20,
                      fontWeight: FontWeight.w800),
                ),
                Divider(),
                Text(
                  "May",
                  style: TextStyle(
                      color: Colors.black54,
                      fontSize: 20,
                      fontWeight: FontWeight.w800),
                ),
                Divider(),
                Text(
                  "June",
                  style: TextStyle(
                      color: Colors.black54,
                      fontSize: 20,
                      fontWeight: FontWeight.w800),
                ),
                Divider(),
                Text(
                  "August",
                  style: TextStyle(
                      color: Colors.black54,
                      fontSize: 20,
                      fontWeight: FontWeight.w800),
                ),
                Divider(),
              ],
            ),
            alignment: Alignment.center,

            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30.0),
                topRight: Radius.circular(30.0),
              ),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 5,
                  blurRadius: 7,
                  offset: Offset(0, 3), // changes position of shadow
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
