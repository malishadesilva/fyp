import 'package:flutter/material.dart';
import 'package:fyp/utils/database.dart';

class CostByCategory extends StatefulWidget {
  const CostByCategory({Key key}) : super(key: key);

  @override
  _CostByCategoryState createState() => _CostByCategoryState();
}

class _CostByCategoryState extends State<CostByCategory> {
  @override
  void initState() {
    m();
    // TODO: implement initState
    super.initState();
  }

  List<dynamic> foodCostList = [];

  List<dynamic> medicineCostList = [];
  List<dynamic> transportCostList = [];
  List<dynamic> utilityCostList = [];
  List<dynamic> otherTaskData = [];

  String medicineCost;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.refresh_outlined),
          onPressed: () => m(),
        ),
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.green),
          title: Text("Cost Analysis by Category" ,style: TextStyle(color: Colors.green, fontSize: 23),),
          backgroundColor: Colors.white,
        ),
        body: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.all(18.0),
              child: Text(
                "Medicine Cost",
                style: TextStyle(color: Colors.purple, fontSize: 23),
              ),
            ),
            medicineCostList.length == 0
                ? Text(" ")
                : ListView.builder(
                    shrinkWrap: true,
                    itemCount: medicineCostList.length,
                    itemBuilder: (context, index) {
                      return Card(
                        margin: EdgeInsets.all(15),
                        color: Colors.purple[100],
                        elevation: 2,
                        child: ListTile(
                          leading: Text((index + 1).toString()),
                          title:
                              Text('${medicineCostList[index][2].toString()}'),
                          subtitle: Column(
                            // mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                  'Date : ${medicineCostList[index][1].toString()}'),
                            ],
                          ),
                          trailing: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text(
                                '${medicineCostList[index][0].toString()}',
                                style: TextStyle(
                                    color: Colors.black, fontSize: 20),
                              )
                              // IconButton(onPressed: () {}, icon: Icon(Icons.edit)),
                            ],
                          ),
                        ),
                      );
                    }),
            foodCostList.length != 0 ? Divider(color: Colors.green) : Text(" "),
            foodCostList.length != 0
                ? Padding(
                    padding: const EdgeInsets.all(18.0),
                    child: Text(
                      "Food Cost",
                      style: TextStyle(color: Colors.amber, fontSize: 23),
                    ),
                  )
                : Text(" "),
            foodCostList.length == 0
                ? Text(" ")
                : ListView.builder(
                    shrinkWrap: true,
                    itemCount: foodCostList.length,
                    itemBuilder: (context, index) {
                      return Card(
                        margin: EdgeInsets.all(15),
                        color: Colors.yellow[100],
                        elevation: 2,
                        child: ListTile(
                          leading: Text((index + 1).toString()),
                          title: Text(foodCostList[index][2].toString()),
                          subtitle: Column(
                            // mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                  'Date : ${foodCostList[index][1].toString()}'),
                            ],
                          ),
                          trailing: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text(
                                '${foodCostList[index][0].toString()}',
                                style: TextStyle(
                                    color: Colors.black, fontSize: 20),
                              )
                              // IconButton(onPressed: () {}, icon: Icon(Icons.edit)),
                            ],
                          ),
                        ),
                      );
                    }),

//utility
            utilityCostList.length != 0
                ? Divider(color: Colors.green)
                : Text(" "),
            utilityCostList.length != 0
                ? Padding(
                    padding: const EdgeInsets.all(18.0),
                    child: Text(
                      "Utility Cost",
                      style: TextStyle(color: Colors.red, fontSize: 23),
                    ),
                  )
                : Text(" "),

            utilityCostList.length == 0
                ? Text(" ")
                : ListView.builder(
                    shrinkWrap: true,
                    itemCount: transportCostList.length,
                    itemBuilder: (context, index) {
                      return Card(
                        margin: EdgeInsets.all(15),
                        color: Colors.red[100],
                        elevation: 2,
                        child: ListTile(
                          leading: Text((index + 1).toString()),
                          title: Text(transportCostList[index][2].toString()),
                          subtitle: Column(
                            // mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                  'Date : ${transportCostList[index][1].toString()}'),
                              //Text(transportCostList[index][0].toString()),
                            ],
                          ),
                          trailing: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text(
                                '${transportCostList[index][0].toString()}',
                                style: TextStyle(
                                    color: Colors.black, fontSize: 20),
                              )
                              // IconButton(onPressed: () {}, icon: Icon(Icons.edit)),
                            ],
                          ),
                        ),
                      );
                    }),

            utilityCostList.length != 0
                ? Divider(color: Colors.green)
                : Text(" "),
            utilityCostList.length != 0
                ? Padding(
                    padding: const EdgeInsets.all(18.0),
                    child: Text(
                      "Transport Cost",
                      style: TextStyle(color: Colors.red, fontSize: 23),
                    ),
                  )
                : Text(" "),

            utilityCostList.length == 0
                ? Text(" ")
                : ListView.builder(
                    shrinkWrap: true,
                    itemCount: utilityCostList.length,
                    itemBuilder: (context, index) {
                      return Card(
                        margin: EdgeInsets.all(15),
                        color: Colors.red[100],
                        elevation: 2,
                        child: ListTile(
                          leading: Text((index + 1).toString()),
                          title: Text(utilityCostList[index][2].toString()),
                          subtitle: Column(
                            // mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                  'Date : ${utilityCostList[index][1].toString()}'),
                              //Text(transportCostList[index][0].toString()),
                            ],
                          ),
                          trailing: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text(
                                '${utilityCostList[index][0].toString()}',
                                style: TextStyle(
                                    color: Colors.black, fontSize: 20),
                              )
                              // IconButton(onPressed: () {}, icon: Icon(Icons.edit)),
                            ],
                          ),
                        ),
                      );
                    }),

//other

            otherTaskData.length != 0
                ? Divider(color: Colors.green)
                : Text(" "),
            otherTaskData.length != 0
                ? Padding(
                    padding: const EdgeInsets.all(18.0),
                    child: Text(
                      "Other Cost",
                      style: TextStyle(color: Colors.amber, fontSize: 23),
                    ),
                  )
                : Text(" "),

            otherTaskData.length == 0
                ? Text(" ")
                : ListView.builder(
                    shrinkWrap: true,
                    itemCount: otherTaskData.length,
                    itemBuilder: (context, index) {
                      return Card(
                        margin: EdgeInsets.all(15),
                        color: Colors.green[100],
                        elevation: 2,
                        child: ListTile(
                          leading: Text((index + 1).toString()),
                          title: Text(otherTaskData[index][2].toString()),
                          subtitle: Column(
                            // mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                  'Date : ${otherTaskData[index][1].toString()}'),
                              Text(otherTaskData[index][0].toString()),
                            ],
                          ),
                          trailing: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text(
                                '${otherTaskData[index][0].toString()}',
                                style: TextStyle(
                                    color: Colors.black, fontSize: 20),
                              )
                              // IconButton(onPressed: () {}, icon: Icon(Icons.edit)),
                            ],
                          ),
                        ),
                      );
                    }),
          ],
        ));
    ;
  }

  void m() async {
    medicineCostList = await Database.readMedicineCost();
    transportCostList = await Database.readTransportCost();
    foodCostList = await Database.readFoodCost();
    utilityCostList = await Database.readUtilityCost();
    otherTaskData = await Database.readOtherCost();

    setState(() {
      this.medicineCostList = medicineCostList;
      this.transportCostList = transportCostList;
      this.foodCostList = foodCostList;
      this.utilityCostList = utilityCostList;
      this.otherTaskData = otherTaskData;
    });
    print('vvvv $medicineCostList');
  }
}
