import 'dart:math';

import 'package:flutter/material.dart';
import 'package:fyp/utils/database.dart';

class DistanceAnalysis extends StatefulWidget {
  const DistanceAnalysis({Key key}) : super(key: key);

  @override
  _DistanceAnalysisState createState() => _DistanceAnalysisState();
}

class _DistanceAnalysisState extends State<DistanceAnalysis> {
  String distance ;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.green),
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text("Total Distance of Tasks" , style:TextStyle(color: Colors.green)),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
           
           
            Text(
              distance == null ? "Calculate total distance of all tasks" : "$distance",
              style: TextStyle(fontSize: distance == null ? 16 : 30),
            ),
          
          ElevatedButton.icon(onPressed: () => distanceCal(), icon: Icon(Icons.map_rounded), label: Text ('Calculate Total Distance'))
           
          ],
        ),
      ),
    );
  }

  void distanceCal() async {
    double calculateDistance(lat1, lon1, lat2, lon2) {
      var p = 0.017453292519943295;
      var c = cos;
      var a = 0.5 -
          c((lat2 - lat1) * p) / 2 +
          c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
      return 12742 * asin(sqrt(a));
    }

    List<dynamic> data = [
      // {"lat": 44.968046, "lng": -94.420307},
      // {"lat": 44.33328, "lng": -89.132008},
      // {"lat": 33.755787, "lng": -116.359998},
      // {"lat": 33.844843, "lng": -116.54911},
      // {"lat": 44.92057, "lng": -93.44786},
      // {"lat": 44.240309, "lng": -91.493619},
      // {"lat": 44.968041, "lng": -94.419696},
      // {"lat": 44.333304, "lng": -89.132027},
      // {"lat": 33.755783, "lng": -116.360066},
      // {"lat": 33.844847, "lng": -116.549069},
    ];
    data = await Database.readLanLon();
    double totalDistance = 0;
    for (var i = 0; i < data.length - 1; i++) {
      totalDistance += calculateDistance(data[i]["lat"], data[i]["lng"],
          data[i + 1]["lat"], data[i + 1]["lng"]);
    }
    setState(() {
      print(totalDistance);
      totalDistance = totalDistance / 1000;
      String x = '${totalDistance.toStringAsFixed(2)} km';
      this.distance =x ;
    });
  }
}
