import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fyp/res/custom_colors.dart';
import 'package:fyp/screenss/analysis_screens/cost_analysis.dart';
import 'package:fyp/screenss/analysis_screens/cost_by_category.dart';
import 'package:fyp/screenss/analysis_screens/distance_analysis.dart';
import 'package:fyp/screenss/analysis_screens/get_distance.dart';
import 'package:fyp/screenss/analysis_screens/prdication_screen.dart';
import 'package:fyp/screenss/analysis_screens/shortest_distance_analysis.dart';
import 'package:fyp/screenss/cost_predict.dart';

import 'package:fyp/screenss/map_screen.dart';
import 'package:fyp/widgets/app_bar_title.dart';
import 'package:fyp/widgets/database/crud/db_item_list.dart';

import 'db_add_screen.dart';

class DbDashboardScreen extends StatefulWidget {
  @override
  _DbDashboardScreenState createState() => _DbDashboardScreenState();
}

class _DbDashboardScreenState extends State<DbDashboardScreen> {
  final FocusNode _nameFocusNode = FocusNode();
  final FocusNode _emailFocusNode = FocusNode();
  final FocusNode _passwordFocusNode = FocusNode();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: Container(
          color: Colors.teal,
          child: Column(
            children: [
              SizedBox(
                height: 20,
              ),
              Container(
                height: MediaQuery.of(context).size.height * 0.20,
                width: MediaQuery.of(context).size.width,
                color: Colors.white,
                child: Stack(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(18.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            'PEPO',
                            style: TextStyle(color: Colors.black, fontSize: 20),
                          ),
                          new SvgPicture.asset('assets/images/smartphone.svg',
                              height: 200, width: 100),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              GestureDetector(
                onTap: () =>
                    Navigator.push(context, MaterialPageRoute(builder: (_) {
                  return DistanceAnalysis();
                })),
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                  child: Row(
                    children: [
                      Icon(
                        Icons.calculate,
                        color: Colors.white,
                        size: 30,
                      ),
                      Text(
                        ' Total Distace',
                        style: TextStyle(color: Colors.white, fontSize: 18),
                      )
                    ],
                  ),
                ),
              ),

//Shortest
              GestureDetector(
                onTap: () =>
                    Navigator.push(context, MaterialPageRoute(builder: (_) {
                  return ShortestDistanceAnalysis();
                })),
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                  child: Row(
                    children: [
                      Icon(
                        Icons.map_outlined,
                        color: Colors.white,
                        size: 30,
                      ),
                      Text(
                        ' Shortest distance of tasks',
                        style: TextStyle(color: Colors.white, fontSize: 18),
                      )
                    ],
                  ),
                ),
              ),

              GestureDetector(
                onTap: () =>
                    Navigator.push(context, MaterialPageRoute(builder: (_) {
                  return GetDistance();
                })),
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                  child: Row(
                    children: [
                      Icon(
                        Icons.nordic_walking,
                        color: Colors.white,
                        size: 30,
                      ),
                      Text(
                        ' Get Distance ',
                        style: TextStyle(color: Colors.white, fontSize: 18),
                      )
                    ],
                  ),
                ),
              ),

              Divider(
                color: Colors.white,
              ),

              GestureDetector(
                onTap: () =>
                    Navigator.push(context, MaterialPageRoute(builder: (_) {
                  return CostByCategory();
                })),
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                  child: Row(
                    children: [
                      Icon(
                        Icons.calculate,
                        color: Colors.white,
                        size: 30,
                      ),
                      Text(
                        '  Cost Analize by Category ',
                        style: TextStyle(color: Colors.white, fontSize: 18),
                      )
                    ],
                  ),
                ),
              ),

              GestureDetector(
                onTap: () =>
                    Navigator.push(context, MaterialPageRoute(builder: (_) {
                  return CostAnalysis();
                })),
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                  child: Row(
                    children: [
                      Icon(
                        Icons.calculate,
                        color: Colors.white,
                        size: 30,
                      ),
                      Text(
                        '  Cost Analize ',
                        style: TextStyle(color: Colors.white, fontSize: 18),
                      )
                    ],
                  ),
                ),
              ),

              GestureDetector(
                onTap: () =>
                    Navigator.push(context, MaterialPageRoute(builder: (_) {
                  return PredictionScreen();
                })),
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                  child: Row(
                    children: [
                      Icon(
                        Icons.batch_prediction,
                        color: Colors.white,
                        size: 30,
                      ),
                      Text(
                        ' Predication ',
                        style: TextStyle(color: Colors.white, fontSize: 18),
                      )
                    ],
                  ),
                ),
              ),

              Divider(
                color: Colors.white,
              ),
              GestureDetector(
                onTap: () =>
                    Navigator.push(context, MaterialPageRoute(builder: (_) {
                  return GetDistance();
                })),
                child: Padding(
                  padding: const EdgeInsets.only(top: 20, left: 15),
                  child: Row(
                    children: [
                      Icon(
                        Icons.info,
                        color: Colors.white,
                        size: 20,
                      ),
                      Text(
                        ' About PEPO ',
                        style: TextStyle(color: Colors.white, fontSize: 15),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      backgroundColor: const Color(0xFFF5F5F5),
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.green, size: 40),
        elevation: 0,
        backgroundColor: const Color(0xFFF5F5F5),
        title: AppBarTitle(
          sectionName: 'PEPO',
        ),
      ),
      floatingActionButton: FloatingActionButton(
        tooltip: 'Add Tasks',
        onPressed: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => DbAddScreen(),
            ),
          );
        },
        backgroundColor: Palette.firebaseOrange,
        child: Icon(
          Icons.add,
          color: Colors.white,
          size: 32,
        ),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(
            left: 16.0,
            right: 16.0,
            bottom: 20.0,
          ),
          child: DbItemList(),
        ),
      ),
    );
  }
}
