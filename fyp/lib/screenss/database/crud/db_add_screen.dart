import 'package:flutter/material.dart';
import 'package:fyp/res/custom_colors.dart';
import 'package:fyp/widgets/app_bar_title.dart';
import 'package:fyp/widgets/database/crud/db_add_item_form.dart';


class DbAddScreen extends StatelessWidget {
  final FocusNode _titleFocusNode = FocusNode();
  final FocusNode _descriptionFocusNode = FocusNode();


  

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        _titleFocusNode.unfocus();
        _descriptionFocusNode.unfocus();
      },
      child: Scaffold(
        resizeToAvoidBottomInset:false,
        backgroundColor:Colors.white,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.green),
          elevation: 0,
          backgroundColor: Colors.white,
          title: AppBarTitle(
            sectionName: 'Add Task',
          ),
        ),
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.only(
              left: 16.0,
              right: 16.0,
              bottom: 20.0,
            ),
            child: DbAddItemForm(
                titleFocusNode: _titleFocusNode,
                descriptionFocusNode: _descriptionFocusNode,
              
            ),
          ),
        ),
      ),
    );
  }
}


//green  app bar 
//svg 





//Tititle 

// image // 
//food
//medicine
//utility 
//transport 
//other


