import 'package:cloud_firestore/cloud_firestore.dart';

final FirebaseFirestore _firestore = FirebaseFirestore.instance;
final CollectionReference _mainCollection = _firestore.collection('notes');

class Database {
  static String userUid;

  static Future<void> addItem({
    String title,
    String description,
    DateTime date,
    double latitude,
    double longitude,
    String address,
    String country,
    String postalcode,
    double cost,
  }) async {
    DocumentReference documentReferencer =
        _mainCollection.doc(userUid).collection('items').doc();

    Map<String, dynamic> data = <String, dynamic>{
      "title": title,
      "description": description,
      "date": date,
      "latitude": latitude,
      "logitude": longitude,
      "address": address,
      "country": country,
      "postalcode": postalcode,
      "cost": cost,
    };

    await documentReferencer
        .set(data)
        .whenComplete(() => print("Note item added to the database"))
        .catchError((e) => print(e));
  }

// add location
  static Future<void> addAddLocation({Map<String, dynamic> data, docId}) async {
    DocumentReference documentReferencer =
        _mainCollection.doc(userUid).collection('items').doc(docId);

    await documentReferencer
        .update(data)
        .whenComplete(() => print("Note item updated in the database"))
        .catchError((e) => print(e));
  }

  static Future<void> updateItem({
    String title,
    String description,
    String docId,
  }) async {
    DocumentReference documentReferencer =
        _mainCollection.doc(userUid).collection('items').doc(docId);

    Map<String, dynamic> data = <String, dynamic>{
      "title": title,
      "description": description,
    };

    await documentReferencer
        .update(data)
        .whenComplete(() => print("Note item updated in the database"))
        .catchError((e) => print(e));
  }

  static Stream<QuerySnapshot> readItems() {
    CollectionReference notesItemCollection =
        _mainCollection.doc(userUid).collection('items');

    return notesItemCollection.snapshots();
  }

  static Future<void> deleteItem({
    String docId,
  }) async {
    DocumentReference documentReferencer =
        _mainCollection.doc(userUid).collection('items').doc(docId);

    await documentReferencer
        .delete()
        .whenComplete(() => print('Note item deleted from the database'))
        .catchError((e) => print(e));
  }

  static Future<List<dynamic>> readLanLon() async {
    List<dynamic> data = [];
    await _mainCollection
        .doc(userUid)
        .collection('items')
        .get()
        .then((QuerySnapshot querySnapshot) {
      querySnapshot.docs.forEach((doc) {
        // List<dynamic> data = [
        //   {"lat": 44.968046, "lng": -94.420307},
        //   {"lat": 44.33328, "lng": -89.132008}
        // ],
        // print(doc["latitude"]);
        // print(doc["logitude"]);

        data.add({
          "lat": doc["latitude"],
          "lng": doc["logitude"],
          "address": doc["address"],
          "country": doc["country"],
          "cost": doc["cost"],
          "date": doc["date"],
          "title": doc["title"],
          "description": doc["description"]
        });
      });
      // print("fefefewfw $data");
    });
    return data;
  }

  static Future<List<dynamic>> readMedicineCost() async {
    print("fff");
    List<dynamic> data = [];
    List<dynamic> dt = [];
    await _mainCollection
        .doc(userUid)
        .collection('items')
        .where("description", isEqualTo: "Medicine")
        .get()
        .then((QuerySnapshot querySnapshot) {
      querySnapshot.docs.forEach((doc) {
        data.add(doc["cost"]);
        dt.add([doc["cost"], doc['date'].toDate(), doc['title']]);
      });
    });
    return dt;
  }

  static Future<List<dynamic>> readFoodCost() async {
    print("fff");
    List<dynamic> data = [];
    List<dynamic> dt = [];
    await _mainCollection
        .doc(userUid)
        .collection('items')
        .where("description", isEqualTo: "Food")
        .get()
        .then((QuerySnapshot querySnapshot) {
      querySnapshot.docs.forEach((doc) {
        data.add(doc["cost"]);
        dt.add([doc["cost"], doc['date'].toDate(), doc['title']]);
      });
    });
    return dt;
  }

  static Future<List<dynamic>> readTransportCost() async {
    List<dynamic> data = [];
    List<dynamic> dt = [];
    await _mainCollection
        .doc(userUid)
        .collection('items')
        .where("description", isEqualTo: "Transport")
        .get()
        .then((QuerySnapshot querySnapshot) {
      querySnapshot.docs.forEach((doc) {
        data.add(doc["cost"]);
        dt.add([doc["cost"], doc['date'].toDate(), doc['title']]);
      });
    });
    return dt;
  }

  //readUtilityCost
   static Future<List<dynamic>> readUtilityCost() async {
    List<dynamic> data = [];
    List<dynamic> dt = [];
    await _mainCollection
        .doc(userUid)
        .collection('items')
        .where("description", isEqualTo: "Utility")
        .get()
        .then((QuerySnapshot querySnapshot) {
      querySnapshot.docs.forEach((doc) {
        data.add(doc["cost"]);
        dt.add([doc["cost"], doc['date'].toDate(),  doc['title']]);
      });
    });
    return dt;
  }

  static Future<List<dynamic>> readOtherCost() async {
    List<dynamic> data = [];
    List<dynamic> dt = [];
    await _mainCollection
        .doc(userUid)
        .collection('items')
        .where("description", isEqualTo: "Other")
        .get()
        .then((QuerySnapshot querySnapshot) {
      querySnapshot.docs.forEach((doc) {
        data.add(doc["cost"]);
        dt.add([doc["cost"], doc['date'].toDate(),  doc['title']]);
      });
    });
    return dt;
  }
}
