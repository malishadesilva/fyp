import 'package:flutter/material.dart';
import 'package:fyp/pick_location_google_api.dart';

import 'package:fyp/res/custom_colors.dart';
import 'package:fyp/utils/database.dart';
import 'package:fyp/utils/db_validator.dart';
import 'package:fyp/widgets/get_location.dart';

import '../../custom_form_field.dart';

class DbAddItemForm extends StatefulWidget {
  final FocusNode titleFocusNode;
  final FocusNode descriptionFocusNode;

  const DbAddItemForm({
    this.titleFocusNode,
    this.descriptionFocusNode,
  });

  @override
  _DbAddItemFormState createState() => _DbAddItemFormState();
}

class _DbAddItemFormState extends State<DbAddItemForm> {
  Map<String, dynamic> mapData;
  double latitude = 0.0;
  double longitude = 0.0;
  String country = "";
  String postalCode = "";
  String addressLocation = "";
  double cost = 0.0;
  String selectedUsers = "";
  Item selectedUser;

  void parentChange(data) {
    setState(() {
      mapData = data;
      print("map data is  $mapData");

      this.latitude = mapData['latitude'];
      this.longitude = mapData['longitude'];
      this.country = mapData['country'];
      this.postalCode = mapData['postalcode'].toString();
      this.addressLocation = mapData['address'].toString();

      this.cost = 10.0;
    });
  }

  DateTime selectedDate = DateTime.now();

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  final _addItemFormKey = GlobalKey<FormState>();

  bool _isProcessing = false;

  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();
  final TextEditingController _costController = TextEditingController();

  List<Item> taskCategories = <Item>[
    const Item(
        'Food',
        Icon(
          Icons.food_bank,
          color: const Color(0xFF167F67),
        )),
    const Item(
        'Medicine',
        Icon(
          Icons.medical_services,
          color: const Color(0xFF167F67),
        )),
    const Item(
        'Utility',
        Icon(
          Icons.format_indent_decrease,
          color: const Color(0xFF167F67),
        )),
    const Item(
        'Transport',
        Icon(
          Icons.emoji_transportation,
          color: const Color(0xFF167F67),
        )),
    const Item(
        'Other',
        Icon(
          Icons.flag,
          color: const Color(0xFF167F67),
        )),
  ];

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _addItemFormKey,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(
              left: 8.0,
              right: 8.0,
              bottom: 24.0,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 24.0),
                Text(
                  'Title',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 22.0,
                    letterSpacing: 1,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 8.0),
                CustomFormField(
                  isLabelEnabled: false,
                  controller: _titleController,
                  focusNode: widget.titleFocusNode,
                  keyboardType: TextInputType.text,
                  inputAction: TextInputAction.next,
                  validator: (value) => DbValidator.validateField(
                    value: value,
                  ),
                  label: 'Title',
                  hint: 'Enter your note title',
                ),
                SizedBox(height: 24.0),
                Text(
                  'Type',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 22.0,
                    letterSpacing: 1,
                    fontWeight: FontWeight.bold,
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 5.0),
                  child: Container(
                    margin: const EdgeInsets.all(5.0),
                    padding:
                        const EdgeInsets.symmetric(horizontal: 50, vertical: 5),
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.blueAccent),
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                    ),
                    child: DropdownButton<Item>(
                      hint: Text(
                        "Select Task Category",
                        style: TextStyle(fontSize: 20),
                      ),
                      value: selectedUser,
                      onChanged: (Item Value) {
                        setState(() {
                          selectedUser = Value;
                        });
                      },
                      items: taskCategories.map((Item user) {
                        return DropdownMenuItem<Item>(
                          value: user,
                          child: Row(
                            children: <Widget>[
                              user.icon,
                              SizedBox(
                                width: 35,
                              ),
                              Text(
                                user.name,
                                style: TextStyle(
                                    fontSize: 22, color: Colors.black),
                              ),
                            ],
                          ),
                        );
                      }).toList(),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    TextButton.icon(
                        onPressed: () => _selectDate(context),
                        icon: Icon(Icons.calendar_today),
                        label: Text(
                          'Select date',
                          style: TextStyle(
                              color: Colors.blueAccent,
                              fontSize: 16,
                              fontWeight: FontWeight.bold),
                        )),
                    Text(
                      "${selectedDate.toLocal()}".split(' ')[0],
                      style: TextStyle(
                          color: Colors.black87,
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                SizedBox(height: 15.0),
                //location
                RaisedButton(
                  shape: StadiumBorder(
                    side: BorderSide(color: Colors.lightBlue[900], width: 2),
                  ),
                  onPressed: () => {
                    showModalBottomSheet(
                        isScrollControlled: true,
                        enableDrag: false,
                        context: context,
                        builder: (context) {
                          return Column(
                            children: <Widget>[
                              ListTile(
                                leading: new Icon(Icons.location_pin),
                                title: new Text('Set Location'),
                                onTap: () {
                                  Navigator.pop(context);
                                },
                              ),
                              Container(
                                height: MediaQuery.of(context).size.height - 60,
                                child:
                                    GetLocation(customeFuction: parentChange),
                              )
                            ],
                          );
                        })
                  },
                  color: Colors.lightBlue[900],
                  padding: EdgeInsets.all(12.0),
                  child: Row(
                    //crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment
                        .center, // Replace with a Row for horizontal icon + text
                    children: <Widget>[
                      Icon(Icons.location_pin, color: Colors.white),
                      SizedBox(
                        width: 20,
                      ),
                      Text(
                        "Add Location",
                        style: TextStyle(fontSize: 18, color: Colors.white),
                      )
                    ],
                  ),
                ),
                SizedBox(height: 5.0),
                Container(
                  width: double.infinity,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      addressLocation,
                      style: TextStyle(color: Colors.green),
                    ),
                  ),
                ),
//cost add
                SizedBox(height: 25.0),
                Text(
                  'Task Cost',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 22.0,
                    letterSpacing: 1,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 8.0),
                CustomFormField(
                  isLabelEnabled: false,
                  controller: _costController,
                  //focusNode: widget.titleFocusNode,
                  keyboardType: TextInputType.number,
                  // inputAction: TextInputAction.next,
                  validator: (value) => DbValidator.validateField(
                    value: value,
                  ),
                  label: 'Task Cost',
                  hint: 'Pleasse enter the cost may be for this task',
                ),
              ],
            ),
          ),
          _isProcessing
              ? Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(
                      Palette.firebaseOrange,
                    ),
                  ),
                )
              : Container(
                  width: double.maxFinite,
                  child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(Colors.green),
                      shape: MaterialStateProperty.all(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                    onPressed: () async {
                      widget.titleFocusNode.unfocus();
                      widget.descriptionFocusNode.unfocus();

                      if (_addItemFormKey.currentState.validate()) {
                        setState(() {
                          _isProcessing = true;
                        });

//malisha
                        await Database.addItem(
                            title: _titleController.text,
                            description: selectedUser.name,
                            date: selectedDate.toLocal(),
                            latitude: this.latitude,
                            longitude: this.longitude,
                            address: this.addressLocation,
                            country: this.country,
                            postalcode: this.postalCode,
                            cost: double.parse(_costController.text));

                        setState(() {
                          _isProcessing = false;
                        });

                        Navigator.of(context).pop();
                      }
                    },
                    child: Padding(
                      padding: EdgeInsets.only(top: 14.0, bottom: 14.0),
                      child: Text(
                        'ADD ITEM',
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                          letterSpacing: 2,
                        ),
                      ),
                    ),
                  ),
                ),
        ],
      ),
    );
  }
}

class Item {
  const Item(this.name, this.icon);
  final String name;
  final Icon icon;
}
