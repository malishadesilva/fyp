import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:fyp/res/custom_colors.dart';
import 'package:fyp/screenss/database/crud/db_edit_screen.dart';
import 'package:fyp/utils/database.dart';

class DbItemList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: Database.readItems(),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return Text('Something went wrong');
        } else if (snapshot.hasData || snapshot.data != null) {
          return ListView.separated(
            separatorBuilder: (context, index) => SizedBox(height: 16.0),
            itemCount: snapshot.data.docs.length,
            itemBuilder: (context, index) {
              var noteInfo =
                  snapshot.data.docs[index].data() as Map<String, dynamic>;
              String docID = snapshot.data.docs[index].id;
              String title = noteInfo['title'];
              String description = noteInfo['description'];
              String address = noteInfo['address'];
              //DateTime date = noteInfo['date'];

              return Ink(
                decoration: BoxDecoration(
                  color: description == "Medicine"
                      ? Colors.purple[100]
                      : description == "Food"
                          ? Colors.yellow[100]
                          : description == "Utility"
                              ? Colors.blue[100]
                              : description == "other"
                                  ? Colors.green[100]
                                  : description == "transport"
                                      ? Colors.orange[100]
                                      : Colors.red[100],
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: ListTile(
                  hoverColor: Colors.amber,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  onTap: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => DbEditScreen(
                        currentTitle: title,
                        currentDescription: description,
                        documentId: docID,
                      ),
                    ),
                  ),
                  leading: description == "Medicine"
                      ? Icon(Icons.medical_services,
                          color: const Color(0xFF167F67), size: 28)
                      : description == "Food"
                          ? Icon(Icons.food_bank,
                              color: const Color(0xFF167F67), size: 28)
                          : description == "Utility"
                              ? Icon(Icons.format_indent_decrease,
                                  color: const Color(0xFF167F67), size: 28)
                              : description == "other"
                                  ? Icon(Icons.location_city)
                                  : description == "transport"
                                      ? Icon(Icons.directions_bus,
                                          color: const Color(0xFF167F67),
                                          size: 28)
                                      : Icon(Icons.flag,
                                          color: const Color(0xFF167F67),
                                          size: 28),
                  title: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: Row(
                      children: [
                        Text(
                          '$title | ',
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(color: Colors.black, fontSize: 20),
                        ),
                        Text(
                          description,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(color: Colors.black, fontSize: 14),
                        ),
                      ],
                    ),
                  ),
                  subtitle: Wrap(
                    children: [
                      Icon(
                        Icons.location_on,
                        color: Colors.black45,
                        size: 16,
                      ),
                      Container(
                        width: 200,
                        child: Text(
                          address,
                          style: TextStyle(color: Colors.black54),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          );
        }

        return Center(
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(
              Palette.firebaseOrange,
            ),
          ),
        );
      },
    );
  }
}
