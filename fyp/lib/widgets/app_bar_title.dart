import 'package:flutter/material.dart';


import 'package:fyp/res/custom_colors.dart';

class AppBarTitle extends StatelessWidget {
  final String sectionName;

  const AppBarTitle({
    Key key,
    this.sectionName,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        // Image.asset(
        //   'assets/firebase_logo.png',
        //   height: 20,
        // ),
        SizedBox(width: 8),
        Text(
          'PEPO',
          style: TextStyle(
            color: Color(0xFF057017),
            fontSize: 18,
          ),
        ),
        Text(
          ' Smart',
          style: TextStyle(
            color: Color(0xFF058019),
            fontSize: 18,
          ),
        ),
      ],
    );
  }
}
