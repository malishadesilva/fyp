import 'package:flutter/material.dart';

import 'package:geolocator/geolocator.dart';
import 'package:geocoder/geocoder.dart' as geoCo;
import 'package:google_maps_flutter/google_maps_flutter.dart';

//import 'package:search_map_place/search_map_place.dart';
class GetLocation extends StatefulWidget {
  final String text = "x";
  final customeFuction;
  GetLocation({this.customeFuction});
  @override
  _GetLocationState createState() => _GetLocationState();
}

class _GetLocationState extends State<GetLocation> {
  ///Get current location
  Geolocator geolocator = Geolocator();
  double latitude;
  double longitude;
  String address = "";

  String addressLocation;
  String country;
  String postalCode;

  String _address = "";

  Position position;

  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  GoogleMapController _googleMapController;
  Marker _origin;
  Marker _destination;

  static final CameraPosition _kLake = CameraPosition(
      bearing: 192.8334901395799,
      target: LatLng(6.9271, 79.8612),
     // tilt: 59.440717697143555,
      zoom: 14.4746);

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
       appBar: AppBar(),
      body: Column(children: [
        Padding(
          padding: const EdgeInsets.only(top: 15),
          child: SizedBox(
            height: 600,
            child: GoogleMap(
              trafficEnabled: true,
              compassEnabled: true,
              initialCameraPosition: _kLake,
              // CameraPosition(
              //   target: LatLng(position.latitude.toDouble(),
              //       position.longitude.toDouble()),
              //   zoom: 19.151926040649414,
              // ),
              onMapCreated: (controller) => _googleMapController = controller,
              markers: Set<Marker>.of(markers.values),
              //     if(_origin != null)_origin,
              //     if(_destination != null)_destination,
              // },
              onLongPress: addMarker,
              onTap: (tapped) async {
                final coordinated =
                    new geoCo.Coordinates(tapped.latitude, tapped.longitude);
                getMarker(tapped.latitude, tapped.longitude);
                var address = await geoCo.Geocoder.local
                    .findAddressesFromCoordinates(coordinated);
                var firstAddress = address.first;
                Map<String, dynamic> data = {
                  "latitude": tapped.latitude,
                  'longitude': tapped.longitude,
                  'address': firstAddress.addressLine,
                  'country': firstAddress.countryName,
                  'postalcode': firstAddress.postalCode
                };

                // setState(() {
                //   latitude = tapped.latitude;
                //   longitude = tapped.longitude;
                //   //address = firstAddress.addressLine;
                //   country = firstAddress.countryName;
                //   postalCode = firstAddress.postalCode;
                //   addressLocation = firstAddress.addressLine;
                // });
                // print(latitude);
                // print(longitude);
                // print(country);
                // print(postalCode);
                // print(addressLocation);
                Navigator.pop(context);
                widget.customeFuction(data);
                print("adaD");
              },
            ),
          ),
        ),
      ]),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Theme.of(context).primaryColor,
        foregroundColor: Colors.black,
        onPressed: () => _googleMapController.animateCamera(
            CameraUpdate.newCameraPosition(_kLake)),
        child: Icon(Icons.center_focus_strong),
      ),
    );
  }

  void addMarker(LatLng pos) {
    if (_origin == null || (_origin != null && _destination != null)) {
      setState(() {
        _origin = Marker(
            markerId: MarkerId("origin"),
            infoWindow: const InfoWindow(title: 'Origin'),
            icon: BitmapDescriptor.defaultMarkerWithHue(
                BitmapDescriptor.hueGreen),
            position: pos);
        _destination = null;
      });
    } else {
      setState(() {
        _destination = Marker(
            markerId: MarkerId("Destination"),
            infoWindow: const InfoWindow(title: 'Destination'),
            icon:
                BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue),
            position: pos);
      });
    }
  }

  void getMarker(double lat, double long) {
    MarkerId markerId = MarkerId(lat.toString() + long.toString());
    Marker _marker = Marker(
      markerId: markerId,
      position: LatLng(lat, long),
      icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueCyan),
      infoWindow: InfoWindow(snippet: addressLocation),
    );
    setState(() {
      markers[markerId] = _marker;
    });
  }

  void getCurrentPosition() async {
    Position currentPosition =
        await GeolocatorPlatform.instance.getCurrentPosition();

    setState(() {
      position = currentPosition;
    });
  }

  @override
  void initState() {
    super.initState();
    getCurrentPosition();
  }
}
