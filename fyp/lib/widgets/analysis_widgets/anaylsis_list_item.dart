import 'package:flutter/material.dart';

class AnaylysisListItem extends StatelessWidget {
  final String title;
  final double amount;
  const AnaylysisListItem(
      {@required this.title,
      this.amount,});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            title,
            style: TextStyle(
                color: Colors.black54,
                fontSize: 18,
                fontWeight: FontWeight.w800),
          ),
          Text(
           amount == null ? "" :  amount.toString(),
            style: TextStyle(
                color: Colors.orange,
                fontSize: 16,
                fontWeight: FontWeight.w800),
          ),
        ],
      ),
    );
  }
}
