import 'package:flutter/material.dart';

class Palette {
  static final Color firebaseNavy = Color(0xFFD3D3D3);
  static final Color firebaseOrange = Color(0xFF080502);
  static final Color firebaseAmber = Color(0xFF181510);
  static final Color firebaseYellow = Color(0xFF13120F);
  static final Color firebaseGrey = Color(0xFF272727);
  static final Color googleBackground = Color(0xFFA5A5A5);
}
